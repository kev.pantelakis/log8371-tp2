#!/bin/bash

HTTP_URL="0.0.0.0:8080"

spam=0
case $1 in 
    2)
    spam=10
    ;;
    3)
    spam=40
    ;;
    4)
    spam=400
    ;;
    1)
    spam=5
    ;;
esac

for (( i=0; i < $spam; ++i ))
do
    curl -X POST "$HTTP_URL/algorithm/NaiveBayes" -H  "accept: text/uri-list"  -H  "Content-Type: multipart/form-data" -F "file=@weather.numeric.arff" -F "batchSize=100" -F "useKernelEstimator=0" -F "useSupervisedDiscretization=0" -F "validation=CrossValidation" -F "validationNum=10" --silent > /dev/null &
done
