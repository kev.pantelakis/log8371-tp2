# log8371-tp2

## Q2) Préparation de Weka REST
Installations préalables:
- Docker (avec un compte DockerHub)
- Maven
- JProfiler (GUI)
- ...

Installation de Weka REST (Linux):
- `cd jguwekarest`
- `mvn clean package`
- `docker build -t dockerhubuser/jguweka:OAS3 .`

Valider que l'installation est faîtes:
- `docker images`

Exécuter le conteneur:
- `docker pull mongo; docker run --name mongodb -d mongo`
- `docker run -p 8080:8080 -p 8849:8849 --link mongodb:mongodb dockerhubuser/jguweka:OAS3`

Le REST peut maintenant être utilisé depuis http://0.0.0.0:8080

Ces étapes sont basés sur le [README](jguwekarest/doc/DockerImageDeployment.md) fourni par WekaREST.

## Q3) JProfiler

Le remote Jprofiler contenu dans le conteneur est accessible à l'adresse localhost:8849

1. Sur la machine Host, télécharger et installer [JProfiler.](https://www.ej-technologies.com/download/jprofiler/files) Un redémarage de votre ordinateur sera peut-être nécessaire.
2. Exécuter les conteneurs mongo et jguweka (Q2). 
3. Ouvrir JProfiler version GUI: New session -> attach to remote JVM (IP: localhost:8849)
4. Rouler le script shell de test dans le dossier test: `./runLoadTest.sh [LOAD]` ou LOAD est compris entre [0-3].
- Example: `./runLoadTest.sh 3`

## Q4) JMeter

1. Ouvrir JMeter
2. File -> open -> test/jmetric_load_tests.jmx
3. Dans l'arborescence, aller sur *User Defined Variables* et modifier la variable *file_path_1* pour le path complet du fichier situé dans */test/weather.numeric.arff* .
4. Appuyer sur le bouton play

## Q5) Monitoring

1. Rouler la commande suivante pour commancer l'environnement `docker-compose up -d`
2. Commande pour scale: 
    - `docker-compose scale jguweka=X` où X est le nombre de slaves
    - `docker-compose restart lb` pour recommancer le loadbalencer
3. Pour fermer le docker compose: `docker-compose down`

Les sites webs pertinents:
- Monitoring: http://localhost:8079/containers/
- Weka REST:  http://localhost/
